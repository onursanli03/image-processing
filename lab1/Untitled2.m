close all;
clear all;
clc;


t= 0:0.1:40;

x = 10* exp(-t);


plot(t,x,'r-');
xlabel('Time');
ylabel('x(t) = 10* exp(-t)');