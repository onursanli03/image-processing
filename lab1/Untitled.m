close all;
clear all;
clc;
f = 5;
t=0:0.01:1;

x = cos(2*pi*f*t);
y = cos((2*pi*f)*t+(pi/2));

subplot(2,1,1);
plot(t,x,'r-');
xlabel('Time');
ylabel('x(t) = cos(2*pi*f*t)');
subplot(2,1,2);
plot(t,y,'b-');
xlabel('Time');
ylabel('y(t) = cos((2*pi*f)*t+(pi/2))');

