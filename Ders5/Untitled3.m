clear all;
close all;
clc;


f =double(imread('building.tif'));

g45 = [-2 -1 0; -1 0 1; 0 1 2];
gx = [ -1 -2 -1; 0 0 0; 1 2 1];
gy = [-1 0 1; -2 0 2; -1 0 1];

fx= imfilter(f,gx);

fy=imfilter(f,gy);

f45 = imfilter(f,g45);

Tx = 0.3*max(abs(fx(:)));
fx = fx>=Tx;

Ty = 0.3*max(abs(fy(:)));
fy = fy>=Ty;

T45 = 0.3*max(abs(f45(:)));
f45 = f45>=T45;

imshow(fx,[]);
figure;
imshow(fy,[]);
figure;
imshow(f45,[]);
