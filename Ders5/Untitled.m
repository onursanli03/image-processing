close all;
clear all;
clc;

s = 1;
m = 0;

f=imread('Test.tif');

x = -3*s:3*s;
g = 1/(sqrt(2*pi)*s)*exp(-1*(x.^2)/(2*s.^2));   
g2 = g'*g;
toplam = sum(sum(g2));
g2 = g2/toplam; 
fout = imfilter(f,g2);
figure;
imshow(fout);
   


