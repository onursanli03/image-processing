clear all;
close all;
clc;

for i = 1:8
    x = -3*i:0.2:3*i;
    y = x;
    [X,Y] = meshgrid(x,y); 
    a = exp(-((X.^2)+(Y.^2))/(2*(i^2)));
    subplot(2,4,i);
    mesh(a);
end