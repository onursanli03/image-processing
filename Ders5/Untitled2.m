close all;
clear all;
clc;

k=1.4;
f = imread('moon.tif');
imshow(f);
s = 5;
w= ones(s);
w= (1/(s*s))*w;
fp =imfilter(f,w);
fmask = double(f)- double(k)*double(fp);
g=double(f)+fmask;
figure;
imshow(g,[]);
figure;
imshow(fmask,[]);
