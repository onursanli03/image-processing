a=9;
f=imread('test.tif');
imshow(f);
w=ones(a);
% w(2,2)=4;
% w(1,2)=2;
% w(2,1)=2;
% w(2,3)=2;
% w(3,2)=2;
% w=w/16;
% g=imfilter(f,w,'replicate');
Gmin=ordfilt2(f,1,ones(a,a));
Gmax=ordfilt2(f,a*a,ones(a,a));
Gmedian=ordfilt2(f,median(1:a*a),ones(a,a));

figure
imshow(Gmin);
figure
imshow(Gmax);
figure
imshow(Gmedian);