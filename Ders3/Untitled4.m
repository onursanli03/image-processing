f=imread('washed.tif');
[M N] = size(f);

X =1:256;
H=zeros(1,256);

for i = 1:M
    for j = 1:N
        a=f(i,j)+1;
        H(a)=H(a)+1;
    end
end

plot(X,H);
figure;
imshow(f);