close all;
clear all;
clc;


a = imread('washedPolen.tif'); %reading the image
subplot(2,2,1);
imshow(a);
x = 1:256;
[M N]= size(a); %image size

fre = zeros(256,1); %fre counts the repetition of each pixel
prob = zeros(256,1); %The probability of each repetition.
cum = zeros(256,1);  %sum of cumulative.
probCum = zeros(256,1); %cumulative dist probability.
roundedValues = zeros(256,1);
histogramImg = uint8(zeros(M,N)); %we study with 3-bit pixel 
numOfPix = M*N;

for i = 1:M
    for j =1:N
        d = a(i,j);
        fre(d+1) = fre(d+1) + 1; %calculating counts the repetition of each pixel
        prob(d+1) = fre(d+1)/numOfPix; %calculating probability of each repetition.
    end
end

%graph the before the equalization of image.

%figure;
subplot(2,2,2);
plot(x,fre);

%The cumulative distribution probability
sum = 0;
for i = 1:size(prob)
    sum = sum + fre(i);
    cum(i) = sum;
    probCum(i)=cum(i)/numOfPix; %calculating cumulative distribution probability
    roundedValues(i) = round(probCum(i)*255); %round the values
end

for i=1:M
    for j=1:N
        histogramImg(i,j)=roundedValues(a(i,j)+1); %replacing rounded values histogram image
    end
end

%figure;
subplot(2,2,3);
imshow(histogramImg);

[K, L] = imhist(histogramImg);
%graph after the equalization
%figure;
subplot(2,2,4);
plot(L,K);








