%histogram and histogram equalization

f=imread('washedPolen.tif');
imshow(f);
%[count x]=imhist(f);

[M N]=size(f);
h=zeros(1,256);

for i=1:M
    for j=1:N
        a=f(i,j);
        h(a+1)=h(a+1)+1;
    end
end

%----------------Hist Equalization
figure
x=1:256;
plot(x,h);

Tpix=M*N;
h=h/Tpix;

CumT(1)=h(1);
for i=2:256
    CumT(i)=CumT(i-1)+h(i);
end
TransVal=round(CumT*255);

for i=1:M
    for j=1:N
        a=f(i,j);
        Tf(i,j)=uint8(TransVal(a+1));
    end
end
figure
imshow(Tf);

[Th,Tx]=imhist(Tf);
figure
plot(Tx,Th);

