x = linspace(-15,15,200);
a = 2
b = 5
y1 = 1/(sqrt(2*pi)*a)*exp(-x.^2/(2*a.^2));
y2 = 1/(sqrt(2*pi)*b)*exp(-x.^2/(2*b.^2));

plot(x,y1,'red-'),grid on;
figure;
plot(x,y2,'blue.'),grid on;