close all;
clear all;
clc;

f = imread('Images/spot_shaded_text_image.tif');
imshow(f),figure;
ff = im2bw(f);
fi = imcomplement(ff);

imshow(fi),figure;

se = strel('disk',1);

g1=imopen(fi,se);
imshow(g1),figure;

g2 = imclose(fi,se);
imshow(g2),figure;

g3 = imdilate(fi,se);
imshow(g3);
