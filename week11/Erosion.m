close all;
clear all;
clc;

f = imread('Images/noisy_fingerprint.tif');
ff = im2bw(f);
fi = imcomplement(ff);
imshow(fi);
SE = strel('square',3);
OutputImage1 = imerode(fi,SE);
OutputImage2 = imdilate(fi,SE);

figure;
imshow(OutputImage1);
figure;
imshow(OutputImage2);