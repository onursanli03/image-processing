close all;
clear all;
clc;

f = imread('Images/dark_blobs_on_light_background.tif');
ff = im2bw(f);
fi = imcomplement(ff);

imshow(fi),figure;

SE = strel('disk',7);

g1 = imerode(fi,SE);

imshow(g1);

g2 = imdilate(fi,SE);

figure;
imshow(g2);