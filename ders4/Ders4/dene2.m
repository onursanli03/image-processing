f=imread('fingerprint.tif');
imshow(f);
[M N]=size(f);
T=0.5*(double(min(f(:)))+double(max(f(:))));
done=false
sayac=0;
while ~done
    g=f>=T;
    Tnext=0.5*(mean(f(g))+mean(f(~g)));
    done=abs(T-Tnext)<0.5;
    T=Tnext;
    sayac=sayac+1;
end
figure;
imshow(g);
