clear all;
s=4;
x=-4*s:0.1:4*s;
y=-4*s:0.1:4*s;
for i=1:(80*s+1)
    for j=1:(80*s+1)
        g(i,j)=exp(-(x(i)*x(i)+y(j)*y(j))/(2*s*s));
        lg(i,j)=(x(i)*x(i)+y(j)*y(j)-(2*s*s))/(s^4)*exp(-(x(i)*x(i)+y(j)*y(j))/(2*s*s));
    end
end
mesh(g);
figure
mesh(lg);
