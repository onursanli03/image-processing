f=imread('building.tif');
imshow(f);
f=double(f);

ka=sqrt(2*pi);
sigma=4;
ku=2*(sigma^2);
x=-12:12;
g1=1/(ka*sigma)*exp(-(x.^2)/ku);
figure;
plot(x,g1);

gx=imfilter(f,g1);
figure;
imshow(gx, []);

g1=g1';
gxy=imfilter(gx,g1);
figure;
imshow(gxy,[]);


