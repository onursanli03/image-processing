sigma=1.75;
x=-3*sigma:0.2:3*sigma;
sigma1=sigma;
sigma2=1;
g1=exp(-(x.^2)/(2*sigma1^2))/(sqrt(2*pi)*sigma1);
g1B2=g1'*g1;

g2=exp(-(x.^2)/(2*sigma2^2))/(sqrt(2*pi)*sigma2);
g2B2=g2'*g2;


DOGmatris=g2B2-g1B2;
mesh(DOGmatris)

