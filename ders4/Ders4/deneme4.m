f=imread('building.tif');
imshow(f);

wx=[-1 0 1;
    -2 0 2;
    -1 0 1];
wy=[-1 -2 -1;
    0 0 0 ;
    1 2 1];

fx=imfilter(f,wx);
figure
imshow(fx);

fy=imfilter(f,wy);
figure
imshow(fy);

M=(double(fx).^2+double(fy).^2);

M=sqrt(double(M));
imshow(M);
T=max(max(M (: )));
G= M >= T*0.40;
figure
imshow(G)