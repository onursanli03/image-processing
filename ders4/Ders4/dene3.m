sigma=0.1;
x=-3*sigma:0.2:3*sigma;
y=x;
[X Y]=meshgrid(x,y);
G=exp(-((X.^2)+(Y.^2))/(2*sigma^2));
mesh(X,Y,G);

LG=-(((X.^2)+(Y.^2)-(2*(sigma^2)))/(sigma^4)).*exp(-((X.^2)+(Y.^2))/(2*sigma^2));
figure;
mesh(X,Y,LG);
toplam=sum(LG(:));
LG=LG/toplam;
f=imread('building.tif');
ff=imfilter(f,LG,'replicate');
figure;
imshow(ff);
g=edge(ff,'zerocross');
figure;
imshow(g)