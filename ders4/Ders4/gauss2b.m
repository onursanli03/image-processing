f=imread('building.tif');
imshow(f);
f=double(f);


sigma=4;
ku=2*(sigma^2);
sigma4=sigma^4;

for x=1:25
    xx=x-13;
    for y=1:25
        yy=y-13;
        g2(x,y)=exp(-((xx^2)+(yy^2))/ku);
        lgauss2(x,y)=((xx^2)+(yy^2)-ku)/sigma4*exp(-((xx^2)+(yy^2))/ku);
    end
end


figure;
mesh(g2);
figure;
mesh(lgauss2);
ff=double(f)
glog=imfilter(ff,lgauss2);
ga=abs(glog);
gm=max(ga(:));
gout=ga>(gm*0.2);
figure;
imshow(glog);
figure;
imshow(gout);