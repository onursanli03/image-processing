%% Week 4: Thresholding
%Find the average of max and min
%Threshold all the image on threshold
%calculate the mean of thresholded and not thresholded pixels
%make it new threshold
%repeat until threshold difference is smaller than 0.5


y=imread('images/fingerprint.tif');

h=[];

T=0.5*(double(min(y(:)+max(y(:)))));

h=[h T];

done=false;
i=1;
g=f;

while ~done
    subplot(1,2,i);
    imshow(g);
    title(T);
    
    g= f>=T;
    Tnext=0.5*(mean(f(g))+mean(f(~g)));
    
    done=abs(T-Tnext)<0.5;
    T=Tnext;
    
    h=[h T];
    
    i=i+1;
end

%% Segmentation
%Edge detection:
    %By first derivative
    %take the derivative:
    %df/dx=gx and df/dy=gy
    %calculate the magnitude: M=sqrt(gx^2+gy^2)
    %calculate the direction: Theta=atan(gy/gx);
    %T: Threshold
    %g= 1 M>T
    %   0 M<T

    %By second derivative
    %calculate the laplacian:
    %LF=[d^2f/dx^2 d^2f/dy^2];
    %apply zero crossing operator
    
    
%% Point detection by laplacian filter

f=imread('images/turbine.tif')

w=[-1 -1 -1;
    -1 8 -1;
    -1 -1 -1];

g=abs(imfilter(double(f),w));
T=max(g(:));
g= g>=T;
imshow(g);
    
%% Laplacian of 2D gaussian function

s=5;
step=1;
lim=20;
[x y]=meshgrid(-lim:step:lim, -lim:step:lim);

% 1D gaussian function
G= @(x) 1/(sqrt(pi)*s)*exp(-x^2/2/(s^2));
% plot(arrayfun(G,-5:0.1:5));

% 2D gaussian function without constant parameters
G2= @(x,y) exp(-(x^2+y^2)/2/(s^2));

% First derivative of 2D gaussian function:
DG2= @(x,y) -(x+y)/(s^2)*exp(-(x^2+y^2)/2/(s^2));

% Laplacian of 2D Gaussian function(LoG):
LG2= @(x,y) -(x^2+y^2-2*s^2)/(s^4)*exp(-(x^2+y^2)/2/(s^2));

%calculation of results:
g=arrayfun(G2,x,y);     %gaussian
g1=arrayfun(DG2,x,y);   %first derivative of gaussian
g2=arrayfun(LG2,x,y);   %second derivative of gaussian

% plotting of the results
subplot(1,3,1);
mesh(x,y,g);
title('2D Gaussian function');

subplot(1,3,2);
mesh(x,y,g1);
title('Derivative of 2D Gaussian function');

subplot(1,3,3);
mesh(x,y,g2);
title('Laplacian of 2D Gaussian function');

fix(g2*27)