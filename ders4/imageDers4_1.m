close all;
clear all;
clc;

f = imread('washed.tif');
subplot(2,2,1);
imshow(f);
T= 0.5*(double(min(f(:)))+double(max(f(:))));

done = false;
count = 0;
while ~done
    g= f>=T;
    Tnext=0.5*(mean(f(g))+mean(f(~g)));
    done=abs(T-Tnext)<0.5;
    T=Tnext;
    count = count+1;
end
subplot(2,2,2);
imshow(g);