close all;
clear all;
clc;

a = [3 5 9];

f = imread('test.tif');
subplot(2,2,1);
imshow(f);
title('input image');


for i=1:3
    
    g = ordfilt2(f,1,ones(a(i)));

    subplot(2,2,2);
    imshow(g);
    title('min');

    h = ordfilt2(f,a(i)*a(i),ones(a(i)));

    subplot(2,2,3);
    imshow(h);
    title('max');

    k = ordfilt2(f,median(1:a(i)*a(i)),ones(a(i)));
    subplot(2,2,4);
    imshow(k);
    title('median');
end