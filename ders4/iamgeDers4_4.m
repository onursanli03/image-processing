f=imread('moon.tif');

imshow(f);
figure;

w = [1 1 1; 1 -8 1; 1 1 1];

g1 = imfilter(f,w);
imshow(g1);
figure;

g = f-g1;

imshow(g);