x = 0:0.1:2*pi;
y1 = sin(x);
y2 = cos(x);
subplot(1,3,1);
plot(x,y1,'r -');
grid on;
title('Safok2');
xlabel('Time Series');
ylabel('Sinus and Cosinus');
legend('sin');
hold on;
subplot(1,3,2);
plot (x,y2,'g -');
grid on;
title('Safok1');
xlabel('Time Series');
ylabel('Sinus and Cosinus');
legend('cos');
hold on;
y3 = exp(-x).*x;
subplot(1,3,3);
plot(x,y3,'b -');
grid on;
title('Safok3');
xlabel('Time Series');
ylabel('Exp(-x)*x');
%%axis([0 4 -1 0.4]);
legend('exp');