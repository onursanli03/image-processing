I = imread('cameraman.tif');
[M N]=size(I);
imshow(I),
scale=2;

%aTN=[1 0 0; 0 1 0;-M/2 -N/2 1];
aR=[cos(pi/4) -sin(pi/4) 0; sin(pi/4) cos(pi/4) 0; 0 0 1]
aTP=[1 0 0; 0 1 0;M/2 N/2 1];
%TA=aR*aTN;

T = maketform('affine',aR);

%I2 = imtransform(I,T,'XData', [1 M+M/2], 'YData', [1 N+N/2]);
I2 = imtransform(I,T);
figure, imshow(I2)

 IR = imrotate(I,25);
 figure, imshow(IR)
 IS = imresize(I, scale);
 figure, imshow(IS)
 
 