close all;
clear all;
clc;

f = imread('building.tif');

imshow(f);

Wx=[-1 -2 -1; 0 0 0; 1 2 1];
Wy=[-1 0 1;-2 0 2; -1 0 1];

Gx = imfilter(double(f),Wx);

Gy= imfilter(double(f),Wy);

M = sqrt(Gx.^2 + Gy.^2);

figure;

imshow(M);

M = abs(M(:,:));

T = 0.3*max(M(:));

G= M>=T;
figure;
imshow(G);

aci = atand(Gy./Gx);

figure;
imshow(aci,[]);