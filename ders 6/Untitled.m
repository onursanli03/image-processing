close all;
clear all;
clc;

f =double( imread('turbine.tif'));

w = [-1 -1 -1; -1 8 -1; -1 -1 -1];

g = abs(imfilter(f,w));

T = max(g(:)*0.1);

g = g>=T;

imshow(g);