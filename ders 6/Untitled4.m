close all;
clear all;
clc;

f = double(imread('building.tif'));

s = 3;

x = -3*s:1:3*s;

y = x;

[X, Y] = meshgrid(x,y);

LG = ((X.^2 + Y.^2 - 2*s^2)/(s^4)) .* (exp(-(X.^2 + Y.^2)/(2*s^2)));


G = imfilter(f, LG);

figure;
imshow(G);

G = abs(G(:,:));
T = max(G(:));

Gout= G >=0.3*T;

figure;
imshow(Gout);

a = edge(f,'log',0.3,2);
figure;
imshow(a);

b = edge(f,'sobel',0.3);
figure;
imshow(b);

b = edge(f,'canny',[0.04 0.1],2,25);
figure;
imshow(b);