clear all; 
close all;
clc;

f=double(imread('wirebond.tif'));

w=[2 -1 -1;-1 2 -1; -1 -1 2];

g=imfilter(f,w);

imshow(g,[]);

g = abs(g(:,:));
figure;
imshow(g,[]);

T=max(g(:));

G= g>=T;

figure;
imshow(G,[]);