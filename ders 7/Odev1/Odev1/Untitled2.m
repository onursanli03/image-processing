f=imread('circlesBrightDark.png');
imshow(f);
ff=im2bw(f);
ff=imcomplement(ff);


yeni=ff(3:510 ,3:510);
stats =regionprops('table',yeni,'Centroid','Area','Perimeter','MajorAxisLength','MinorAxisLength')




centers=stats.Centroid;
diameters = mean([stats.MajorAxisLength stats.MinorAxisLength],2);
radii = diameters/2;
hold on;
viscircles(centers,radii);
hold off;
Cevre = 2*pi*radii;