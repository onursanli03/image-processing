clear all;
close all;
clc;


n = -10:10;

u=(n>=0);
u2=(n>=2);

y = n.*u2;

subplot(2,1,1),stem(n,u),title('x[n]'),grid on;
subplot(2,1,2),stem(n,y),title('x[n-2]'),grid on;

%System has memory because non-zero values of y[n] are on the different 'n'
% instant if compare with non-zero values of x[n]

%System is casual because non-zero values of y[n] starts after the non
%zero values of x[n]