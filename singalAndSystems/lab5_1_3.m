close all;
clear all;
clc;

n = 0:10;

x = [1 -1 3 zeros(1,8)];

xh = gauspuls(n);

a = [1];
b = [3 -2 4];

yf = filter(b,a,x);
h = filter(b,a,xh);
yc = conv(x,h);

subplot(4,1,1),stem(n,x),grid on,legend('x[n]');
subplot(4,1,2),stem(n,yf),grid on,legend('yf[n]');
subplot(4,1,3),stem(0:20,yc),grid on,legend('yc[n]');
subplot(4,1,4),stem(n,h),grid on,legend('h[n]');

