clear all;
close all;
clc;

t=-5:0.001:10;

u=heaviside(t); %unit step signal u(t)

u2=heaviside(t-2); %u(t-2)


subplot(2,1,1),plot(t,u);
grid on,ylim([-0.3 1.3]);
subplot(2,1,2),plot(t,u2);
grid on,ylim([-0.3 1.3]) ;



s = gauspuls(t);
s1 = gauspuls(t-2);

figure;

subplot(2,1,1),plot(t,s);
grid on,ylim([-0.3 1.3]);
subplot(2,1,2),plot(t,s1);
grid on,ylim([-0.3 1.3]) ;


rec =rectpuls(t,2);
rec2 = rectpuls(t-2,2);
rec3 = heaviside(t)-heaviside(t-3);

figure;
subplot(3,1,1),plot(t,rec);
grid on,ylim([-0.3 1.3]);
subplot(3,1,2),plot(t,rec2);
grid on,ylim([-0.3 1.3]) ;
subplot(3,1,3),plot(t,rec3);
grid on,ylim([-0.3 1.3]) ;



