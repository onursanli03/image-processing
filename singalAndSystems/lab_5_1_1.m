%lab 5
%question 1

close all;
clear all;
clc;

n=0:9;
x=[1,2,-1 zeros(1,7)];
xh = gauspuls(n);

a=[1 -0.5 0.1];
b=[0.1 -0.5 1];

y = filter(b,a,x);
h = filter(b,a,xh);

subplot(3,1,1),stem(n,x),grid on, legend('x[n]');
subplot(3,1,2),stem(n,y),grid on, legend('y[n]');
subplot(3,1,3),stem(n,h),grid on, legend('h[n]');

%yf and yc are correct  hence, we deal with FIR filters we can use both
%commands conv and filter to compute the output signal