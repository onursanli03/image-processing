clear all;
close all;
clc;

n = -10:10;
n1 = 2;
x1 = (n>=n1); %x1 = u[n-2]
y1 = n.*x1;

n2 = 2+2;
x2 = (n>=n2); %x2 = u[n-4]

y2 = n.*x2

subplot(2,2,1),stem(n,y2),grid on;
subplot(2,2,2),stem(n+2,y1),grid on;

%graphics are not the same thus the system is time varying