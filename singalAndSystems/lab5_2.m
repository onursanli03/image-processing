close all;
clear all;
clc;

n=-5:5;
u=(n>=0);
u2 = (n>=2);

subplot(2,1,1),stem(n,u);
grid on,ylim([-0.3 1.3]);

subplot(2,1,2),stem(n,u2);
grid on,ylim([-0.3 1.3]);

s=gauspuls(n);
s1=gauspuls(n-2);

figure;
subplot(2,1,1),stem(n,s);
grid on,ylim([-0.3 1.3]);

subplot(2,1,2),stem(n,s1);
grid on,ylim([-0.3 1.3]);

rec = rectpuls(n,3);
rec2 = rectpuls(n-1,3);

figure;
subplot(2,1,1),stem(n,rec);
grid on,ylim([-0.3 1.3]);

subplot(2,1,2),stem(n,rec2);
grid on,ylim([-0.3 1.3]);



