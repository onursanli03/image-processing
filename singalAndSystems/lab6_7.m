close all;
clear all;
clc;


nx = 0:5;
nh = 0:5;
ny = 0:10;

x = ones(size(nx)); 

h = nh;

y = conv(x,h);

subplot(3,1,1),stem(nx,x),xlim([-1 11]),grid on;
subplot(3,1,2),stem(nh,h),xlim([-1 11]),grid on;
subplot(3,1,3),stem(ny,y),xlim([-1 11]),grid on;