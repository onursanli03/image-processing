close all;
clear all;
clc;


n = 0:99;
xh = gauspuls(n);

x = 2*cos(pi*n/10);
a = [1];
b = [1 0 -0.5];

%impulse response of the system
y1 = filter(b,a,xh);
%impulse response of the inverse system
y2 = filter(a,b,xh);
%response of the system to pure tone
y3 = filter(b,a,x);
%recovered pure tone  
y4 = filter(a,b,y3);

subplot(6,1,1),stem(n,xh),grid on,legend('xh[n]');
subplot(6,1,2),stem(n,x),grid on,legend('x[n] system input');
subplot(6,1,3),stem(n,y1),grid on,legend('y1[n] impulse response of the system');
subplot(6,1,4),stem(n,y2),grid on,legend('y2[n] inv. system impulse res');
subplot(6,1,5),stem(n,y3),grid on,legend('y3[n] system output');
subplot(6,1,6),stem(n,y4),grid on,legend('y4[n] recovered input');
