close all;
clear all;
clc;

t=-8:0.1:8;
figure
x=lab4_1(t);
plot(t,x), axis([-8 8 -1 3]), grid on
title('x(t)')
figure
y1=x; ty1=t+5;
plot(ty1,y1), axis([ty1(1) ty1(end) -1 3 ]), grid on
title('y_1(t) - (x(t) delayed by 5');

figure
y2=x; ty2=t-5;
plot(ty2,y2), axis ([ ty2(1) ty2(end) -1 3]), grid on 
title('y_2(t) - (x(t) advanced by 5) ' );

figure
y4=-2*x; ty4=(-t+5)/2;
plot(ty4,y4),axis ([ ty4(end) ty4(1) -5 1]), grid on
title('y_4(t) - (-2x(t) advanced by 5 & then time reversed and time scaled');
