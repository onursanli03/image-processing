clear all;
close all;
clc;

t = 0:0.01:9;

x = heaviside(t);%unitstep

xh = gauspuls(t)*100;

a = [1 0.5];

b = [1];

y = lsim(b,a,x,t);

h = lsim(b,a,xh,t);

y2 = step(b,a,t);

h2 = impulse(b,a,t);

figure;
subplot(3,1,1),plot(t,x),grid on,legend('x(t)');
subplot(3,1,2),plot(t,y),grid on,legend('y(t)');
subplot(3,1,3),plot(t,h),grid on,legend('h(t)');

figure;
subplot(2,1,1),plot(t,y2),grid on,legend('y2(t)');
subplot(2,1,2),plot(t,h2),grid on,legend('h2(t)');


