clear all;
close all;
clc;


step = 0.01;

tx1 = 0:step:1.5;
tx2 = (1.5+step):step:4;
tx3 = (4+step):step:10;

x1 = 0.6*ones(size(tx1));

x2 = 0.3*ones(size(tx2));

x3 = zeros(size(tx3));

tx = [tx1 tx2 tx3];

x = [x1 x2 x3];

th = 0:step:10;

h = exp(-th);

ty = 0:step:20;

y = conv(x,h)*step;

subplot(3,1,1),plot(tx,x),grid on;
subplot(3,1,2),plot(th,h),grid on;
subplot(3,1,3),plot(ty,y),grid on;





