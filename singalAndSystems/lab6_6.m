clear all;
close all;
clc;


h = [1 2 3];

x = [1 2 3 4 5];

nh = -1:1;

nx = 0:4;

ny = -1:5;

y = conv(x,h);

subplot(3,1,1),stem(nx,x),xlim([-2 6]),grid on;
subplot(3,1,2),stem(nh,h),xlim([-2 6]),grid on;
subplot(3,1,3),stem(ny,y),xlim([-2 6]),grid on;
