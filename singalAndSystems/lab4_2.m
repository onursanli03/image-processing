N= 20;
n=0:99;
x = cos(2*pi*n/N);
y = cos(2*pi*n/N+pi/2);

stem(n,x,'b');
hold on;
stem(n,y,'r');
title('Discreet signals');