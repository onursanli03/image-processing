close all;
clear all;
clc;

nx=-3:13;
x=[zeros(1,3) 2 0 1 -1 3 zeros(1,9)];
ny1=nx+2;
figure
subplot(2,1,1),stem(nx,x,'filled'), title('x[n]'),xlabel('n'),ylabel
grid on
subplot(2,1,2),stem(nx,x,'filled'), title('x[n]'),xlabel('n'),ylabel
grid on

%(x[2n],x[1/2n] scaling)
x=[2 0 1 -1 3 0];
n=0:length(x)-1;
ny1=n+2;
y2=x(1:3:end);
ny2=0:length(y2)-1;
y3=zeros(2*length(x),1);
y3(1:3:end)=x;
ny3=0:lentgh(y3)-1;
figure,subplot(2,2,1),stem(n,x,'filled'),title('x[n]'),grid on,
axis([-3 13 -2 4])
subplot(222),stem(ny1,x,'filled'),title('x[n-2]'),grid on,
axis([-3 13 -2 4])
figure,subplot(2,2,3),stem(ny2,y2,'filled'),title('x[2n]'),grid on,
axis([-3 13 -2 4])
figure,subplot(2,2,4),stem(ny3,y3,'filled'),title('x[n/2]'),grid on,
axis([-3 13 -2 4])
