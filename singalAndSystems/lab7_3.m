close all;
clear all;
clc;

n = 0:2;

h1 = [2 3 4];
h2 = [-1 3 1];
h3 = [1 1 -1];

hp = h1 + h2;

h = conv(hp,h3);

u = heaviside(n);
u2 = heaviside(n-2);

x = u-u2;

lh = length(h);

figure;

stem(0:lh-1,h),grid on, legend('h[n]'),xlim([-1 9]);

y = conv(x,h);
ly = length(y);


figure;
stem(0:ly-1,y),grid on,legend('y[n]'),xlim([-1 9]);







