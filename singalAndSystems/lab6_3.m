n = -10:10;

x1 = ones(1,length(n));

y1 = n.*x1;

subplot(2,1,1),stem(n,x1),title('x[n]=1');
subplot(2,1,2),stem(n,y1),title('y[n]');

%y[n] increases at n increases without bound thus the system is not stable