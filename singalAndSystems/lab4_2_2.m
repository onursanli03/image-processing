close all;
clear all;
clc;

N= 15;
k = [1 2 5 7];

n=0:2*N;


for i=1:4
    x = sin (2*pi*k(i)*n/N);
    subplot(2,2,i);
    stem(n,x);
end