close all;
clear all;
clc;

tx = 0:0.01:2;

x = ones(size(tx)); %rule1

th1 = 0:0.01:1;

h1 = 1 - th1;

th2 = 1+0.01:0.01:2;

h2 = zeros(size(th2));

th = [th1 th2];

h = [h1 h2];

ty = 0:0.01:4;

y = conv(x,h)*0.01;

subplot(3,1,1),plot(tx,x),grid on;
subplot(3,1,2),plot(th,h),grid on;
subplot(3,1,3),plot(ty,y),grid on;