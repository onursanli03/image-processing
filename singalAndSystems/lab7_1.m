close all;
clear all;
clc;

n= 0:149;
N = 150;

s= 0.1*rand(1,N);
v = 0.2*cos(0.2*pi*n);
x=cos(0.02*pi*n);
x1 = x+s;
x2 = x+v;
a=[1];
b=[0.33 0.33 0.33];

y1 = filter(b,a,x1);
y2 = filter(b,a,x2);
y3 = filter(b,a,x1+x2);

subplot(3,1,1),stem(n,y1),grid on,legend('y1[n]');
subplot(3,1,2),stem(n,y2),grid on,legend('y2[n]');
subplot(3,1,3),stem(n,y3),grid on,legend('y3[n]');


