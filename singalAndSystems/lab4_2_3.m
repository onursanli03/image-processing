N = 6;

n = 0 : 10*N;

x1 = sin(2*pi*n/N);

subplot(2,2,1);
stem(n,x1);

x2 = sin(2*n/N);

subplot(2,2,2);
stem(n,x2);

x3 = (sin(2*pi*n/N))+(cos(2*pi*n/N*2));

subplot(2,2,3);
stem(n,x3);

x4 = sin(14*pi*n/N);

subplot(2,2,4);
stem(n,x4);