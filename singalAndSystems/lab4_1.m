function x=lab4_1(t)

for i=1:length(t)
    if(t(i)>=-1)&&(t(i)<=0)
        x(i)=-t(i)+1;
    elseif(t(i)>=0 ) && (t(i)<=1)
        x(i)=t(i)+1;
    else
        x(i)=0;
    end
end