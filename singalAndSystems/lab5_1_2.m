close all;
clear all;
clc;


n = 0:10;

x = [5,1,1,1,0,0,1,1,1,0,0];

xh = gauspuls(n);

a = [1 -1.1 0.5 0 0.3];
b = [0.5 -0.2];

yf = filter(b,a,x);
h = filter(b,a,xh);
yc = conv(x,h);

subplot(4,1,1),stem(n,x),grid on,legend('x[n]');
subplot(4,1,2),stem(n,yf),grid on,legend('yf[n]');
subplot(4,1,3),stem(n,h),grid on,legend('h[n]');
subplot(4,1,4),stem(0:20,yc),grid on,legend('yc[n]');


%yf is correct
%yc is correct for only between 0 and 11 hence, the best way to deal with
%IIR filters isto describe them by their difference equation and use the
%MATLAB command filter in order to compute the outpur signal
